/* eslint-disable no-useless-constructor */
import React, { Component } from 'react';
class Flowers extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <p>I'm a Tulip {this.props.color}</p>
        );
    }
}
export default Flowers;
