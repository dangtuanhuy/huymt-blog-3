/* eslint-disable no-useless-constructor */
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Trees from '../src/components/Trees/Trees.component'
import Flowers from '../src/components/Flowers/Flowers.component'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Bamboo: [
        { height: 1.5, color: 'cyan'},
        { height: 2.5, color: 'green'},
        { height: 3.5, color: 'brown'},
      ]
    }
}
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
        </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
        </a>
        <button> Switch description of Bamboo
        </button>
        <Trees height="3m" color="cyan"/>
        <Trees height="4m" color="brown"/>
        <Trees height="4.5" color="green">This bamboo is probably more than a few years old</Trees>
        <Flowers  color="red"/>
        </header>
      </div>
    );
  }
}

export default App;
