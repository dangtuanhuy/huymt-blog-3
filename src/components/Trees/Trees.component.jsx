import React from 'react';
const Trees = (props) => {
    return (
        <div>
            <p>
                It is a thousand-year-old bamboo. It is {props.color} and {props.height} high
            </p>
            <p>
                {props.children}
            </p>
        </div>
    )
};
export default Trees;